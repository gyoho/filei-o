#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>



/**Global variable**/
#define BUFFER_SIZE	 50
#define CHILD		 5
#define READ_END     0
#define WRITE_END    1
#define TIME_OUT	 30

/**generate timestamp**/
//This will give you the time in seconds + microseconds
struct timeval start_tv, curr_tv;


/**The SIGALRM interrupt handler**/
// Used with signal function
// When time's out, set timesout to 1
int timeup = 0;
void SIGALRM_handler(int signo) {
    printf("\nTime's up\n");
    timeup = 1;
    exit(0);
}

int main() {
	/**Variables**/	
	// pid and fd for each child
	// (our data are the pid of the child)
	pid_t pid[CHILD];
	int fds[CHILD][2];	// pipe variables


	// flag for 'if pid == 0'
	// all processes have this variable
	// so that they know if itself is child or not
	bool child = false;

	// parent has CHILD # of pipes
	// each child needs to have its own pipe for communicating with parent
	// use child_idx as pipe # to keep track of which pipe to use
	int child_idx = 0;
	int	msg_idx = 0;
	
	/**Start timer**/
	gettimeofday(&start_tv, NULL);
	// For convinience, set microtime of start time to 0
	start_tv.tv_usec = 0;
	//~ printf("start: %d	%d\n", start_tv, start_tv.tv_sec);
	
	

	/**Fork 5 children with pipes**/
	// create the children
	int i;
	int pipe_result;
	for(i=0; i<CHILD; i++) {
		// create the pipe for this child
		// need to open pipe before fork
		pipe_result = pipe(fds[i]);

		if(pipe_result == -1) {
			fprintf(stderr,"pipe failed");
			return 1;
		}

		// fork child
		pid[i] = fork();

		if(pid[i] < 0) {
			fprintf(stderr,"fork failed");
			return 1;
		}

		// setup child state
		if(pid[i] == 0){
			child = true;	// if child, set the flag 'true'
			child_idx = i;	// each child should know his id
			//~ printf("My name is %dth child\n", child_idx);
			// avoid child from forking
			// make sure only parent fork child
			break;
		}
	}
	
	//~ printf("I'm %dth child\n", child_idx);
	
	
	/***Child writes messages***/
	if(child) {
		// Close the unused READ end of the pipe.
		close(fds[child_idx][READ_END]);
		// buffer for each input
		char write_msg[BUFFER_SIZE];
		
		
		/** 5th child **/
		if(child_idx == 4) {			
			//Terminate the 5th child process after 30 seconds
			struct itimerval tval;
			timerclear(& tval.it_interval);
			timerclear(& tval.it_value);
			tval.it_value.tv_sec = 30;
			
			(void) signal(SIGALRM, SIGALRM_handler);
			setitimer(ITIMER_REAL, &tval, NULL);
			
			while(true) {
				printf("Type words\n");
				
				fflush(stdout);
				
				// read from user
				char *user_msg = NULL;
				size_t size;
				// get user input
				getline(&user_msg, &size, stdin);
				
				// get timestamp
				gettimeofday(&curr_tv, NULL);	
				
				// format the messages
				sprintf(write_msg, "0:%d.%0.3d:	Child %d message %s",
						(curr_tv.tv_sec - start_tv.tv_sec),
						(curr_tv.tv_usec)%1000, child_idx+1, user_msg);
				
				// padding to the BUFFER_SIZE		
				sprintf(write_msg,"%-49s",write_msg);

				
				// write the message (with time stamp) to WRITE end of its pipe
				write(fds[child_idx][WRITE_END], write_msg, BUFFER_SIZE);					
			}
		}
		
		/** 1-4th child **/
		else {
			// write messages to the buffer of the WRITE end
			// stop after 30 secs
			gettimeofday(&curr_tv, NULL);			
			
			while(curr_tv.tv_sec - start_tv.tv_sec < TIME_OUT) {
				msg_idx++;				
				// format messages			
				sprintf(write_msg, "0:%d.%0.3d:	Child %d message %d",
						(curr_tv.tv_sec - start_tv.tv_sec),
						(curr_tv.tv_usec)%1000, child_idx+1, msg_idx);
				
				// padding to the BUFFER_SIZE
				sprintf(write_msg,"%-49s",write_msg);
				
				// write to the WRITE end
				write(fds[child_idx][WRITE_END], write_msg, BUFFER_SIZE);
								
				// sleep random time
				sleep(rand()%3);
				
				gettimeofday(&curr_tv, NULL);
				//~ printf("current: %d, start: %d\n", curr_tv, start_tv);
			}
			// close writing end - parent will see an EOL when trying to read more data
			close(fds[child_idx][WRITE_END]);
			printf("Child %d closed pipe\n", child_idx);
			//~ fprintf(fp, "Child %d closed pipe\n", child_idx);
		}
	} 
	
	
	/***Parent read the messages***/
	
	else {
		// file stream for text output
		FILE *fout;
		fout = fopen("output.txt", "w");

		// sets of file descriptors
		fd_set inputs, inputfds;
		// initialize inputfds to the empty set
		FD_ZERO(&inputs);
		
		// for each child
		// close the unused WRITE end of the pipe
		// set the fds for select to monitor
		int k;
		for(k=0; k<CHILD; k++) {
			close(fds[k][WRITE_END]);
			FD_SET(fds[k][READ_END], &inputs);
		}
		
		// buffer for incoming message
	    char read_msg[BUFFER_SIZE];
	    
	    // stop after 30 secs
		gettimeofday(&curr_tv, NULL);	
	
		// use select to read the messages from children
		while(curr_tv.tv_sec - start_tv.tv_sec < TIME_OUT) {
			// register fds to monitor
			// everytime before select()!!
			inputfds = inputs;
			
			// Check the all pipes
			// to see if there is any data in them 
			int select_result;
			select_result = select(FD_SETSIZE, &inputfds, NULL, NULL, NULL);
			//~ printf ("\n\nselect returns %d\n", select_result);

			//   Check the results
			//   No data:  the program loops again.
			//   Got data: print the message in the pipes to a text file
			//   Error:     terminate.
			if(select_result == -1) {
				perror("select failed.\n");
				return 1;
			}
			else if(select_result == 0) {
				// no data is received
				// wait until data is coming
				// No time out
			}
			else {		
				int m;
				for(m=0; m<CHILD; m++){
					//~ printf("m: %d\n", m);

					// FD_ISSET returns '1' if the fds is available
					int fd_isset = FD_ISSET(fds[m][READ_END], &inputfds);
					//~ printf ("fd_isset returns %d\n", fd_isset);
					//~ printf("fds: %d\n", fds[m][READ_END]);
				
					if(fd_isset) {						
						int read_size = 1;
						// Read from the READ end of the pipe.
						//~ while(read_size > 0){
							// get the current time
							gettimeofday(&curr_tv, NULL);
						
							read_size = read(fds[m][READ_END], read_msg, BUFFER_SIZE);
							//~ printf("read_size returns: %d\n", read_size);
							if(read_size > 0){
								//~ printf("0:%d.%d		%s\n", (curr_tv.tv_sec - start_tv.tv_sec),
									//~ (curr_tv.tv_usec), read_msg);
								fprintf(fout, "0:%d.%d		%s\n", (curr_tv.tv_sec - start_tv.tv_sec),
									(curr_tv.tv_usec), read_msg);
							}
						//~ }					
					// Remove already read fds from the fdsets
					// in order to read next fds
					// FD_CLR (fds[m][READ_END], &inputfds);
					}
				}
			}
			// check the timeout
			gettimeofday(&curr_tv, NULL);	
		}
		
		// wait child to finish
		int s, status;
		for(s=0; s<CHILD; s++) {
			waitpid(pid[s], &status, 0);
		}
		
		// Close the 5th child's pipe
		close(fds[4][WRITE_END]);
		printf("Child 5 closed pipe\n");
		
		// close read end - child will see SIGPIPE
		int l;
		for(l=0; l<CHILD; l++) {
			close(fds[l][READ_END]);
		}
		printf("Parent closed pipe\n");
	}
	
	// close the output file stream
	//fclose(fp);
}
