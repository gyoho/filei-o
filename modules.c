#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>


/**generate timestamp**/
//This will give you the time in seconds + microseconds
struct timeval start_tv, curr_tv,;
gettimeofday(&tv,NULL);
start_tv.tv_sec // seconds
start_tv.tv_usec // microseconds
curr_tv.tv_sec // seconds
curr_tv.tv_usec // microseconds

	


/**Terminate the process after 30 seconds**/
struct itimerval tval;
timerclear(& tval.it_interval);
timerclear(& tval.it_value);
tval.it_value.tv_sec = 30;    // 30 seconds timeup
int timeup = 0;

// The SIGALRM interrupt handler.
// Used with signal function
// When time's out, set timesout to 1
void timerHandler(int signo) {
    assert(signo == SIGALRM);
    timeup = 1;
}

// Send alarm when the time's out
signal(SIGALRM, timerHandler);
// Start timer
setitimer(ITIMER_REAL, &tval, NULL);



/**Declaration**/
char buffer[128];
int result, nread;
fd_set inputs, inputfds;	// sets of file descriptors
struct timeval timeup;		// sets timeup


/**Use select()**/
result = select(FD_SETSIZE, &inputfds, NULL, NULL, NULL);	// result
