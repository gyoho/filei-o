#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

#include <iostream>

#define BUFSIZE 1024
#define KIDS    5

int main ()
{
  //////////////////////////////////////////////////////////////////////////////
  // we need a data item, a pipe, and a pid store for each kid
  // (our data are the pid of the child)
  pid_t data[KIDS];
  int   fd  [KIDS][2];
  pid_t pid [KIDS];

  // flag to avoid  'if pid == 0' repetition
  bool child = false;

  // each child needs to know what pipe to use
  int  child_idx = -1;
  //////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////////
  // create the kids
  for ( int k = 0; k < KIDS; k++ )
  {
    // take care that children are not forking again!  Otherwise, we get KIDS!
    // kids ...
    if ( ! child )
    {
      // create the pipe for this kid
      if ( -1 == ::pipe (fd[k]) )
      {
        std::cerr << "pipe failed: " << ::strerror (errno) << std::endl;
        exit (-1);
      }


      // fork kid
      pid[k] = ::fork ();

      if ( pid[k] < 0 ) 
      {
        std::cerr << "fork failed: " << ::strerror (errno) << std::endl;
        exit (-2);
      }

      // setup kid state
      if ( pid[k] == 0 )
      {
        child = true;
        child_idx = k;
      }
    }
  } 
  // all kids forked, all kids have child set to true now, and know their 
  // pipe index
  //////////////////////////////////////////////////////////////////////////////
  

  // say hello
  std::cout << " hello " << ::getpid () << " - " << child << std::endl;


  //////////////////////////////////////////////////////////////////////////////
  // all kids spawned, now gather/collect the data
  if ( child )
  { 
    // close reading end of my pipe set
    ::close (fd[child_idx][0]);

    // write pid to pipe
    char buf[BUFSIZE];
    snprintf (buf, BUFSIZE, "%ld", ::getpid ());

    if ( sizeof (buf) != ::write (fd[child_idx][1], buf, sizeof (buf)) )
    {
      std::cerr << "write failed: " << ::strerror (errno) << std::endl;
      exit (-4);
    }

    std::cout << "write: " << buf << std::endl;

    // close writing end - parent will see an EOL when trying to read more data
    ::close (fd[child_idx][1]);
  } 
  //////////////////////////////////////////////////////////////////////////////

  else  

  //////////////////////////////////////////////////////////////////////////////
  {
    // parent: handle data from all kids

    for ( int k = 0; k < KIDS; k++ )
    {
      // close writing ends of pipes
      ::close (fd[k][1]);

      // read data from pipe
      char buf[BUFSIZE] = "";

      if ( 0 >= ::read (fd[k][0], &buf, sizeof (buf)) )
      {
        std::cerr << "read failed: " << ::strerror (errno) << std::endl;
        exit (-5);
      }

      data[k] = ::atoi (buf);

      std::cout << "read: " << buf << std::endl;

      // close reading end of pipe - child will see SIGPIPE when trying to
      // write after this
      ::close (fd[k][0]);

      // wait for this child to finish
      ::waitpid (pid[k], NULL, 0);
    }


    // parent prints gathered data
    for ( int a = 0; a < KIDS; a++ )
    {
      std::cout << a << ": " <<  data[a] << std::endl;
    }
  }
  //////////////////////////////////////////////////////////////////////////////

  return 0;
}
